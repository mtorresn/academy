import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpModule } from '@angular/http';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { ReactiveFormsModule } from '@angular/forms';

import { AppComponent } from './app.component';
import { DiccionaryComponent } from './dictionary/dictionary.component';
import { SignComponent } from './sign/sign.component';
import { SearchComponent } from './search/search.component';
import { LoaderComponent } from './loader/loader.component';

import { SignsService } from './services/signs.service';
import { ApiService } from './services/api.service';

@NgModule({
  declarations: [
    AppComponent,
    DiccionaryComponent,
    SignComponent,
    SearchComponent,
    LoaderComponent
  ],
  imports: [
    BrowserModule,
    HttpModule,
    NgbModule.forRoot(),
    ReactiveFormsModule
  ],
  providers: [
    ApiService,
    SignsService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
