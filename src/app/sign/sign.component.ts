import { Component, Input } from '@angular/core';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { Sign } from '../interfaces/sign.interface';

@Component({
  selector: 'app-sign',
  templateUrl: './sign.component.html'
})
export class SignComponent {

  @Input() data = null as Sign;

}
