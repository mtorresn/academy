import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { Headers } from '@angular/http';
import { RequestOptions } from '@angular/http';
import { Observable, BehaviorSubject } from 'rxjs/';
import 'rxjs/add/operator/map';


@Injectable()
export class ApiService {
  public _basePath: string;
  private _headers: any;
  private _options: any;
  public _loading = new BehaviorSubject(false) as BehaviorSubject<boolean>;

  constructor(private http: Http) {
    this.http = http;
      this._headers = new Headers();
      this._headers.set('Content-Type', 'application/json');
      this._options = new RequestOptions({headers: this._headers});
      this._basePath = 'http://angular.devlicious.com.mx';
  }

  public get(path: string, params?: any): Observable<any> {
    const paramsSize = params ? Object.keys(params).length : 0;
    let paramsString = '';

    if (paramsSize > 0) {

      paramsString = paramsString === '' ?  '?' : paramsString;
      const keys = Object.keys(params);

      for ( let i = 0; i < paramsSize; i ++ ) {
          const _prefix = i !== 0 ? '&' : '';
          paramsString += `${_prefix}${keys[i]}=${params[keys[i]]}`;
      }
    }

    const url = `${this._basePath}/${path}${paramsString}`;
    this._loading.next(true);
    return this.http
        .get(url, this._options)
    .map(response => {
      this._loading.next(false);
      return response.json();
    });
  }
}
