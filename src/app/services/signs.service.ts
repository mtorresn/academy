import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { ApiService } from '../services/api.service';
import { Sign } from '../interfaces/sign.interface';
import { SearchFilters } from '../interfaces/search.interface';

@Injectable()
export class SignsService {

  constructor(private _api: ApiService) { }

  public _getSigns(_filters?: SearchFilters): Observable<Sign[]> {
    return this._api.get('signs', _filters)
      .map(_signs => {
        return _signs.map(_sign => {
          _sign.image = `${this._api._basePath}${_sign.image}`;
          _sign.categories = this._getCategories(_sign.categories_str);
          return _sign;
        });
      });
  }

  private _getCategories(_categories_str: string): string[] {
    return _categories_str.split(',') || [];
  }
}
