import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { FormControl, FormGroup, FormBuilder } from '@angular/forms';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { SignsService } from '../services/signs.service';
import { SearchFilters } from '../interfaces/search.interface';

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html'
})

export class SearchComponent implements OnInit {

  @Output() result = new EventEmitter();

  public searchForm: FormGroup;
  private _filters= new SearchFilters() as SearchFilters ;

  constructor(private _builder: FormBuilder, private _signsService: SignsService) {
  }

  ngOnInit() {
    this.searchForm = this._builder.group({
      search: ''
    });

    this.searchForm.valueChanges.subscribe(_changes => {
      console.log('Hey you!!! The form is changing!!!!!', _changes);
      this._filters.name = _changes.search;
      this._signsService._getSigns(this._filters).subscribe(_signs => {
        console.log('result', _signs);
        this.result.emit(_signs);
      });
    });
  }

}
