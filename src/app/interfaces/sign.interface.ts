export class Sign {
  name: string;
  image: string;
  description: string;
  language: string;
  categories: string[];
}
