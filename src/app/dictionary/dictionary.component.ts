import { Component, OnInit } from '@angular/core';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { SignsService } from '../services/signs.service';
import { ApiService } from '../services/api.service';
import { Sign } from '../interfaces/sign.interface';

@Component({
  selector: 'app-dictionary',
  templateUrl: './dictionary.component.html',
  styleUrls: ['./dictionary.component.scss']
})
export class DiccionaryComponent implements OnInit {

  public signs$ = new BehaviorSubject([]) as BehaviorSubject<Sign[]>;
  public loading = false as boolean;

  constructor( private _signsService: SignsService, private _apiService: ApiService) {}

  ngOnInit() {
    this._signsService._getSigns().subscribe(_signs => {
        this.updateSigns(_signs);
      });
    this._apiService._loading.subscribe(status => {
      this.loading = status;
    });
  }

  public updateSigns(_signs: Sign[]): void {
    this.signs$.next(_signs);
  }
}
